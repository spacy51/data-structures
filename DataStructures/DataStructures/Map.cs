﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataStructures
{
    /// <summary>
    /// All keys are unique.
    /// No key is null.
    /// </summary>
    /// <typeparam name="K">Type of keys</typeparam>
    /// <typeparam name="V">Type of values</typeparam>
    public class Map<K, V> : IEnumerable<Map<K, V>.Entry>
    {
        public class Entry
        {
            public readonly K Key;
            public readonly V Value;


            public Entry(K key, V value)
            {
                if (key == null)
                {
                    throw new ArgumentNullException("key");
                }

                Key = key;
                Value = value;
            }


            public override bool Equals(object obj)
            {
                return
                    obj is Entry &&
                    Key.Equals(((Entry)obj).Key);
            }


            public override int GetHashCode()
            {
                return Key.GetHashCode();
            }


            public override string ToString()
            {
                return
                    string.Format(
                        "{0} -> {1}",
                        Key,
                        Value);
            }
        }


        private readonly IEnumerable<Entry> entries_;


        public Map(IEnumerable<Entry> entries)
        {
            // TODO: performance: avoid full duplicate check when calling from certain internal methods
            var duplicates =
                from entry1 in entries
                from entry2 in entries
                where
                    entry1 != entry2 &&
                    entry1.Equals(entry2)
                select entry1;

            if (duplicates.Any())
            {
                throw new ArgumentException(
                    "Duplicate entries found",
                    "entries");
            }

            entries_ = entries;
        }


        public Map(params Entry[] entries)
            : this(entries as IEnumerable<Entry>)
        {
        }


        public Map()
            : this(Enumerable.Empty<Entry>())
        {
        }


        public bool ContainsKey(K key)
        {
            return
                entries_.Any(
                    entry => key.Equals(
                        entry.Key));
        }


        public Map<K, V> Add(IEnumerable<Entry> entries)
        {
            return
                new Map<K, V>(
                    entries_.Concat(
                        entries));
        }


        public Map<K, V> Add(params Entry[] entries)
        {
            return
                Add(
                    entries as IEnumerable<Entry>);
        }


        public Map<K, V> Add(K key, V value)
        {
            return
                Add(
                    new Entry(
                        key,
                        value));
        }


        public Map<K, V> Remove(IEnumerable<K> keys)
        {
            return
                new Map<K, V>(
                    entries_.Where(
                        entry => !keys.Any(
                            key => entry.Key.Equals(
                                key))));
        }


        public Map<K, V> Remove(params K[] keys)
        {
            return
                Remove(
                    keys as IEnumerable<K>);
        }


        public IEnumerator<Entry> GetEnumerator()
        {
            return entries_.GetEnumerator();
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return entries_.GetEnumerator();
        }
    }
}
