﻿using System;
using MyMap = DataStructures.Map<int, string>;
using MyEntry = DataStructures.Map<int, string>.Entry;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var map = new MyMap(
                new MyEntry(42, "answer to all questions"),
                new MyEntry(25, "age"),
                new MyEntry(1, "a start"));

            foreach (var entry in map)
            {
                Console.WriteLine(entry);
            }

            Console.WriteLine();

            foreach (var entry in
                map
                .Remove(1)
                .Add(
                    new MyEntry(66, "Jahre"),
                    new MyEntry(11880, "mit dem Blubb")
                    )
                //.Add(42, "duplicate")
                .Remove(42))
            {
                Console.WriteLine(entry);
            }
            Console.ReadKey();
        }
    }
}
